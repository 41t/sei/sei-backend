## Installation

```bash 
 npm install
```
## Prisma

```bash
  #from package ./src/
 npx prisma generate
 #from package ./src/
 npx prisma db push
```
чтобы подключить бд сделайте .env по примеру example.env
DATABASE_URL - ссылка на базу данных
## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```