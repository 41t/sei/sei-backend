import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { JournalService } from './journal.service';
import { CreateJournalDto } from './dto/create-journal.dto';
import { UpdateJournalDto } from './dto/update-journal.dto';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { Journal } from './entities/journal.entity';

@ApiTags('journal')
@Controller('journal')
export class JournalController {
  constructor(private readonly journalService: JournalService) {}

  @Post()
  @ApiOperation({ summary: 'create a new journal' })
  @ApiResponse({ status: 201, description: 'Successful create' })
  async createJournal(
    @Body()
    data: CreateJournalDto
    ): Promise<Journal> {
    return this.journalService.create(data);
  }
  
  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of all journals' })
  @ApiResponse({ status: 200, description: 'Success' })
  findAll() {
    return this.journalService.findMany({});
  }
  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of one journAl' })
  @ApiResponse({ status: 200, description: 'Success' })
  findOne(@Param('id') userid: string) {
    return this.journalService.findOne({ id: +userid });
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'update journal data' })
  @ApiResponse({ status: 200, description: 'Success' })
  update(@Param('id') id: number, @Body() updateJournalDto: UpdateJournalDto) {
    return this.journalService.update({ where: { id }, data: updateJournalDto });
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'delete journal' })
  @ApiResponse({ status: 200, description: 'Resource deleted successfully' })
  remove(@Param('id') id: number) {
    return this.journalService.remove({ id });
  }
}