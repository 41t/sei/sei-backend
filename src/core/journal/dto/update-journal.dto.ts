import { PartialType } from '@nestjs/mapped-types';
import { CreateJournalDto } from './create-journal.dto';
import { Prisma } from '@prisma/client';

export class UpdateJournalDto extends PartialType(CreateJournalDto) implements Prisma.JournalUpdateInput {
    href?: string;
    name?: string;
}
