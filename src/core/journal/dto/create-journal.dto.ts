import { Prisma } from "@prisma/client";

export class CreateJournalDto implements Prisma.JournalCreateInput {
    href: string;
    name: string;
}
