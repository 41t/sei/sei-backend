import { Injectable } from '@nestjs/common';
import { CreateJournalDto } from './dto/create-journal.dto';
import { UpdateJournalDto } from './dto/update-journal.dto';
import { Journal } from './entities/journal.entity';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class JournalService {
  constructor(private prisma: PrismaService) { }

    async findOne(
      where: Prisma.JournalWhereUniqueInput,
    ): Promise<Journal | null> {
      return this.prisma.journal.findUnique({ where });
    }

    async findMany(params: {
      skip?: number;
      take?: number;
      cursor?: Prisma.JournalWhereUniqueInput;
      where?: Prisma.JournalWhereInput;
      orderBy?: Prisma.JournalOrderByWithRelationInput;
    }): Promise<Journal[]> {
      const { skip, take, cursor, where, orderBy } = params;
      return this.prisma.journal.findMany({ skip, take, cursor, where, orderBy });
    }

    async create(data: CreateJournalDto): Promise<Journal> {
      return this.prisma.journal.create({ data });
    }

    async update(params: {
      where: Prisma.JournalWhereUniqueInput;
      data: UpdateJournalDto;
    }): Promise<Journal> {
      const { data, where } = params;
      return this.prisma.journal.update({ data, where });
    }

    async remove(where: Prisma.JournalWhereUniqueInput): Promise<Journal> {
      return this.prisma.journal.delete({ where });
    }
}
