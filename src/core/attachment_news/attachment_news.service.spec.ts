import { Test, TestingModule } from '@nestjs/testing';
import { AttachmentNewsService } from './attachment_news.service';

describe('AttachmentNewsService', () => {
  let service: AttachmentNewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AttachmentNewsService],
    }).compile();

    service = module.get<AttachmentNewsService>(AttachmentNewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
