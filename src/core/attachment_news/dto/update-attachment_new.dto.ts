import { PartialType } from '@nestjs/mapped-types';
import { CreateAttachmentNewDto } from './create-attachment_new.dto';

export class UpdateAttachmentNewDto extends PartialType(CreateAttachmentNewDto) {}
