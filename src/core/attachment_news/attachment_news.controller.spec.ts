import { Test, TestingModule } from '@nestjs/testing';
import { AttachmentNewsController } from './attachment_news.controller';
import { AttachmentNewsService } from './attachment_news.service';

describe('AttachmentNewsController', () => {
  let controller: AttachmentNewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AttachmentNewsController],
      providers: [AttachmentNewsService],
    }).compile();

    controller = module.get<AttachmentNewsController>(AttachmentNewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
