import { Module } from '@nestjs/common';
import { AttachmentNewsService } from './attachment_news.service';
import { AttachmentNewsController } from './attachment_news.controller';

@Module({
  controllers: [AttachmentNewsController],
  providers: [AttachmentNewsService],
})
export class AttachmentNewsModule {}
