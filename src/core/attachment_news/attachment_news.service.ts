import { Injectable } from '@nestjs/common';
import { CreateAttachmentNewDto } from './dto/create-attachment_new.dto';
import { UpdateAttachmentNewDto } from './dto/update-attachment_new.dto';

@Injectable()
export class AttachmentNewsService {
  create(createAttachmentNewDto: CreateAttachmentNewDto) {
    return 'This action adds a new attachmentNew';
  }

  findAll() {
    return `This action returns all attachmentNews`;
  }

  findOne(id: number) {
    return `This action returns a #${id} attachmentNew`;
  }

  update(id: number, updateAttachmentNewDto: UpdateAttachmentNewDto) {
    return `This action updates a #${id} attachmentNew`;
  }

  remove(id: number) {
    return `This action removes a #${id} attachmentNew`;
  }
}
