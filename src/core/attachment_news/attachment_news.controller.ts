import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AttachmentNewsService } from './attachment_news.service';
import { CreateAttachmentNewDto } from './dto/create-attachment_new.dto';
import { UpdateAttachmentNewDto } from './dto/update-attachment_new.dto';

@Controller('attachment-news')
export class AttachmentNewsController {
  constructor(private readonly attachmentNewsService: AttachmentNewsService) {}

  @Post()
  create(@Body() createAttachmentNewDto: CreateAttachmentNewDto) {
    return this.attachmentNewsService.create(createAttachmentNewDto);
  }

  @Get()
  findAll() {
    return this.attachmentNewsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.attachmentNewsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAttachmentNewDto: UpdateAttachmentNewDto) {
    return this.attachmentNewsService.update(+id, updateAttachmentNewDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.attachmentNewsService.remove(+id);
  }
}
