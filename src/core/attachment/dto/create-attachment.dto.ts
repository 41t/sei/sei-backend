import { Prisma } from "@prisma/client";

export class CreateAttachmentDto implements Prisma.AttachmentCreateInput {}
