import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(private userService: UserService, private jwtService: JwtService) {}

  async validateUser(username: string): Promise<any> {
    const user = await this.userService.findOne({login: username});
    if (user) return user;
    throw new UnauthorizedException('ошибка авторизации')
  }

  async login(user: any): Promise<any> {
    const { id, ...userData } = user,
        payload = { ...userData, sub: id };

    return { access_token: this.jwtService.sign(payload) };
  }
}
