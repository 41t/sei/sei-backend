import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { Public } from './public/public.decorator';
import { User } from '../user/entities/user.entity';

@ApiTags('auth')
@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/auth/login')
  @Public()
  @ApiOperation({ summary: 'get jwt for user' })
  @ApiResponse({ status: 201, description: 'Successful authorization' })
  @UseGuards(LocalAuthGuard)
  async login(@Req() req : {user: {username: string, password: string}}) {
    return this.authService.login(req.user);
  }

  @Get('profile')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get authorizated user data (whisuot id)' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  getProfile(@Req() req) {
    return req.user;
  }
}
