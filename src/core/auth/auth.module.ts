import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { AuthController } from './auth.controller';
import { RoleModule } from '../role/role.module';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { NewsModule } from '../news/news.module';
import { JournalModule } from '../journal/journal.module';
import { AttachmentModule } from '../attachment/attachment.module';
import { AttachmentNewsModule } from '../attachment_news/attachment_news.module';

@Module({
  imports: [
    RoleModule, 
    UserModule,
    NewsModule,
    JournalModule,
    AttachmentModule,
    AttachmentNewsModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_EXPIRES_IN }
    })],
  controllers: [AuthController],
  providers: [
    AuthService,
    PrismaService,
    LocalStrategy,
    JwtStrategy,
    UserService],
    exports: [AuthService]
})
export class AuthModule {}
