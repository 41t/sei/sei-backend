import { User as UserModel } from "@prisma/client";
import { ApiProperty } from '@nestjs/swagger';

export class User implements UserModel {
    @ApiProperty({ example: 1, description: 'The user id' })
    id: number;

    @ApiProperty({ example: 'test', description: 'The user login (must be unique)' })
    login: string;

    @ApiProperty({ example: 'test@gmail.com', description: 'The user email (must be unique)' })
    email: string;
    
    @ApiProperty({ description: "User fitst name", required: true, example: "Петр" })
    first_name: string;

    @ApiProperty({ description: "User middle name", required: false, example: "Петрович" })
    middle_name: string;

    @ApiProperty({ description: "User last name", required: true, example: "Петров" })
    last_name: string;
    
    @ApiProperty({ description: "date when user will be created", required: false, example: "01-01-2000" })
    created_at: Date;

    attachmentid: number;

    @ApiProperty({ example: 'skip this', description: 'automatically put user' })
    roleid: number;
}
