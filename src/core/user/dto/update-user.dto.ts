import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { Prisma } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';

export class UpdateUserDto extends PartialType(CreateUserDto)  implements Prisma.UserUpdateInput {
    
    @ApiProperty({ description: "User email", required: false, example: "test@gmail.com" })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    @MaxLength(255, { message: 'Email должен содержать не более 255 символов' })
    @IsEmail({}, { message: 'Некорректный формат email-адреса' })
    email?: string;

    @ApiProperty({ description: "User first name", required: true, example: "Петр" })
    first_name?: string;

    @ApiProperty({ description: "User middle name", required: false, example: "Петрович" })
    middle_name?: string;

    @ApiProperty({ description: "User last name", required: true, example: "Петров" })
    last_name?: string;
    
    @ApiProperty({ description: "date when user will be created", required: false, example: "01-01-2000" })
    created_at?: string | Date;

    attachment?: Prisma.AttachmentCreateNestedOneWithoutUsersInput;
    news?: Prisma.NewsCreateNestedManyWithoutUserInput;
    role?: Prisma.RoleCreateNestedOneWithoutUsersInput;
}
