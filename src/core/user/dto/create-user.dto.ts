import { ApiProperty } from "@nestjs/swagger";
import { Prisma } from "@prisma/client";
import { IsEmail, IsNotEmpty, IsOptional, MaxLength, MinLength } from "class-validator";

export class CreateUserDto implements Prisma.UserCreateInput {
    
    @ApiProperty({ description: "User login", required: true, example: "test" })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    @MinLength(3, { message: 'Логин должен содержать не менее 3 символов' })
    @MaxLength(50, { message: 'Логин должен содержать не более 50 символов' })
    login: string;

    @ApiProperty({ description: "User email", required: true, example: "test@gmail.com" })
    @IsEmail({}, { message: 'Некорректный формат email-адреса' })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    @MaxLength(255, { message: 'Email должен содержать не более 255 символов' })
    email: string;
    
    @ApiProperty({ description: "User first name", required: true, example: "Петр" })
    first_name: string;

    @ApiProperty({ description: "User middle name", required: false, example: "Петрович" })
    @IsOptional()
    middle_name?: string;

    @ApiProperty({ description: "User last name", required: true, example: "Петров" })
    last_name: string;
    
    @ApiProperty({ description: "date when user will be created", required: false, example: "01-01-2000" })
    created_at?: string | Date;

    attachment?: Prisma.AttachmentCreateNestedOneWithoutUsersInput;
    news?: Prisma.NewsCreateNestedManyWithoutUserInput;
    role?: Prisma.RoleCreateNestedOneWithoutUsersInput;
}