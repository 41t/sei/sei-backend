import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, BadRequestException, Logger } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Public } from '../auth/public/public.decorator';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Post()
  @ApiOperation({ summary: 'create a new user' })
  @ApiResponse({ status: 201, description: 'Successful create' })
  async signupUser(
    @Body()
    data: CreateUserDto
    ): Promise<User> {
    if ( (await this.userService.findOne({ login: data.login })) || (await this.userService.findOne({ email: data.email })))
    {
      throw new BadRequestException(HttpStatus.BAD_REQUEST);    
    }
    data.role = {connect:{name: String(data.role)}}
    return this.userService.create(data);
  }
  
  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of all users' })
  @ApiResponse({ status: 200, description: 'Success' })
  findAll() {
    return this.userService.findMany({});
  }
  @Public()
  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of one user' })
  @ApiResponse({ status: 200, description: 'Success' })
  findOne(@Param('id') userid: string) {
    return this.userService.findOne({ id: +userid });
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'update user data' })
  @ApiResponse({ status: 200, description: 'Success' })
  update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update({ where: { id }, data: updateUserDto });
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'delete user' })
  @ApiResponse({ status: 200, description: 'Resource deleted successfully' })
  remove(@Param('id') id: number) {
    return this.userService.remove({ id });
  }
}
