import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { Prisma } from '@prisma/client';
import { News } from './entities/news.entity';

@Injectable()
export class NewsService {
  constructor(private prisma: PrismaService) { }
  async create(data: CreateNewsDto) {
    return this.prisma.news.create({ data });
  }

  async findMany( params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.NewsWhereUniqueInput;
    where?: Prisma.NewsWhereInput;
    orderBy?: Prisma.NewsOrderByWithRelationInput;
  }
  ): Promise<News[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.news.findMany({skip, take, cursor, where, orderBy});
  }

   async findOne(
    where: Prisma.NewsWhereUniqueInput,
  ): Promise<News | null> {
    return this.prisma.news.findUnique({ where });
  }

  async update(params: {
    where: Prisma.NewsWhereUniqueInput;
    data: Prisma.NewsUpdateInput;
  }): Promise<News> {
    const { data, where } = params;
    return this.prisma.news.update({ data, where });
  }

  async remove(where: Prisma.NewsWhereUniqueInput): Promise<News> {
    return this.prisma.news.delete({ where });
  }
}
