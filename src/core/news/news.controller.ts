import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { ApiTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { News } from '@prisma/client';

@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}
  @Post()
  @ApiOperation({ summary: 'create a new news' })
  @ApiResponse({ status: 201, description: 'Successful create' })
  async createNews(
    @Body()
    data: CreateNewsDto
    ): Promise<News> {
    return this.newsService.create(data);
  }
  
  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of all news' })
  @ApiResponse({ status: 200, description: 'Success' })
  findAll() {
    return this.newsService.findMany({});
  }
  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of one news' })
  @ApiResponse({ status: 200, description: 'Success' })
  findOne(@Param('id') userid: string) {
    return this.newsService.findOne({ id: +userid });
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'update news data' })
  @ApiResponse({ status: 200, description: 'Success' })
  update(@Param('id') id: number, @Body() updateNewsDto: UpdateNewsDto) {
    return this.newsService.update({ where: { id }, data: updateNewsDto });
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'delete news' })
  @ApiResponse({ status: 200, description: 'Resource deleted successfully' })
  remove(@Param('id') id: number) {
    return this.newsService.remove({ id });
  }
}

