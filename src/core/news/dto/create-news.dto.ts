import { Prisma } from "@prisma/client";

export class CreateNewsDto implements Prisma.NewsCreateInput{
    name: string;
    text: string;
    anons: string | Date;
    created_at: string | Date;
    updated_at: string | Date;
    user?: Prisma.UserCreateNestedOneWithoutNewsInput;
    attachments?: Prisma.Attachment_NewsCreateNestedManyWithoutNewsInput;
}
