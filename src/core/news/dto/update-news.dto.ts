import { PartialType } from '@nestjs/mapped-types';
import { CreateNewsDto } from './create-news.dto';
import { Prisma } from '@prisma/client';

export class UpdateNewsDto extends PartialType(CreateNewsDto) implements Prisma.NewsUpdateInput {
    name: string;
    text: string;
    anons: string | Date;
    created_at: string | Date;
    updated_at: string | Date;
    user?: Prisma.UserCreateNestedOneWithoutNewsInput;
    attachments?: Prisma.Attachment_NewsCreateNestedManyWithoutNewsInput;
}
