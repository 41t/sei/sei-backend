import { ApiProperty } from "@nestjs/swagger";
import {Role as RoleModel} from "@prisma/client"
export class Role implements RoleModel{
    @ApiProperty({ example: 1, description: 'The role id' })
    id: number;
    @ApiProperty({ example: 'test', description: 'The role name' })
    name: string;
}
