import { ApiProperty } from "@nestjs/swagger";
import { Prisma } from "@prisma/client";
import { IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class CreateRoleDto implements Prisma.RoleCreateInput {
    @ApiProperty({ description: "Role name", nullable: false })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    @MinLength(2, { message: 'Имя должно содержать не менее 2 символов' })
    @MaxLength(50, { message: 'Имя должно содержать не более 50 символов' })
    name: string;
    users?: Prisma.UserCreateNestedManyWithoutRoleInput;
}
