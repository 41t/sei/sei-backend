import { PartialType } from '@nestjs/mapped-types';
import { CreateRoleDto } from './create-role.dto';
import { Prisma } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { MinLength, MaxLength } from 'class-validator';

export class UpdateRoleDto extends PartialType(CreateRoleDto) implements Prisma.RoleUpdateInput {
    @ApiProperty({ description: "Role name", nullable: true })
    @MinLength(2, { message: 'Имя должно содержать не менее 2 символов' })
    @MaxLength(50, { message: 'Имя должно содержать не более 50 символов' })
    name?: string;
}
