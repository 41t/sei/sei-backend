import { Module } from '@nestjs/common';
import { PrismaService } from './prisma/prisma.service';
import { APP_GUARD } from '@nestjs/core';
import { AuthModule } from './core/auth/auth.module';
import { RoleModule } from './core/role/role.module';
import { UserModule } from './core/user/user.module';
import { JwtAuthGuard } from './core/auth/guards/jwt-auth.guard';


@Module({
  imports: [
    RoleModule, 
    UserModule,
    AuthModule,
  ],
  providers: [
    PrismaService, 
      {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    }],
})
export class AppModule {}
